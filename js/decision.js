$(document).ready(function(){

  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  $("form").submit(function(event){
    var choices = $.makeArray($("input")).map(function(elem){
      return $(elem).val();
    }).filter(function(elem){
      return elem.length>0;
    });

    choices.push("Sleep on it");
    choices.sort(function(a, b) {
      return getRandomInt(-1, 2);
    });
    //console.log(choices);

    $("div.answerBox").append("<div class=\"text-center alert alert-info alert-dismissible\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>"
    + choices[getRandomInt(0, choices.length)] + "</div>");

    event.preventDefault();
  });
});
